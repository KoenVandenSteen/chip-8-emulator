#include "emulator.h"
#include <math.h>
#include <stdio.h>
#include <PSGL/psgl.h>

#include <sys/spu_initialize.h>
#include <sys/paths.h>

CGprogram gfxLoadProgramFromSource(CGprofile target, const char* filename)
{
	CGprogram id = cgCreateProgramFromFile(cgCreateContext(), CG_SOURCE, filename, target, NULL, NULL);
	if (!id)
	{
		printf("Failed to load shader program >>%s<< \nExiting\n", filename);
		exit(0);
	}
	else
		return id;
}

bool KeyCheck(int)
{
	return false;
}

int main()
{

	Emulator chip8;

	chip8.s_CurRom = "/app_home/maze.ch8";

	sys_spu_initialize(6, 1);

	psglInit(0);

	PSGLdeviceParameters params;
	params.enable = PSGL_DEVICE_PARAMETERS_WIDTH_HEIGHT;
	params.width = 1280;
	params.height = 720;
	PSGLdevice *device = psglCreateDeviceExtended(&params);

	PSGLcontext *context = psglCreateContext();
	psglMakeCurrent(context, device);
	psglResetCurrentContext();

	glViewport(0, 0, params.width, params.height);

	GLfloat vertices[] = {
		-0.75, +0.75, 0, 0,
		+0.75, +0.75, 1, 0,
		+0.75, -0.75, 1, 1,
		-0.75, -0.75, 0, 1,
	};

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, 4 * sizeof(GLfloat), vertices);

	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 4 * sizeof(GLfloat), vertices + 2);

	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	unsigned int * image = new unsigned int[chip8.GetMemWidth()* chip8.GetMemheight()];

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glClearColor(0.f, 0.1f, 0.f, 1.f);

	cgRTCgcInit();

	CGprogram mVertexProgram = gfxLoadProgramFromSource(CG_PROFILE_SCE_VP_RSX, "/app_home/vs.cg");
	CGprogram mFragmentProgram = gfxLoadProgramFromSource(CG_PROFILE_SCE_FP_RSX, "/app_home/fs.cg");
	
	cgGLBindProgram(mVertexProgram);
	cgGLBindProgram(mFragmentProgram);

	cgGLEnableProfile(CG_PROFILE_SCE_VP_RSX);
	cgGLEnableProfile(CG_PROFILE_SCE_FP_RSX);

	cgGLSetTextureParameter(cgGetNamedParameter(mFragmentProgram, "diffuseMap"), tex);


	for (;;)
	{
		if (Emulator::s_ReadData)
			chip8.ReadInData();

		chip8.GameLoop();

		//const Chip8::u8 * video = chip8.Video();
		//for (int i = 0; i < Chip8::WIDTH * Chip8::HEIGHT; ++i)
		//{
		//	image[i] = video[i] ? 0xFFFFFFFF : 0x000000FF;
		//}
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, chip8.GetMemWidth(), chip8.GetMemheight(), 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, &chip8.GetGraphMem()[0]);

		glClear(GL_COLOR_BUFFER_BIT);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		psglSwap();
	}

	// Destroy the context, then the device (before psglExit)
	//psglDestroyContext(context);
	//psglDestroyDevice(device);

	//psglExit();
}
