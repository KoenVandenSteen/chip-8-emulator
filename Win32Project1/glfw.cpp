#include "glfw.h"
#include "Emulator.h"
#include <iostream>

glfw::glfw():
	m_Emulator(nullptr)
{
	m_Emulator = new Emulator();
	InitializeGLFW();
	SetInputCallBack();
	Loop();
}


glfw::~glfw()
{
	delete m_Emulator;
}


void glfw::InitializeGLFW()
{
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// Create a GLFWwindow object that we can use for GLFW's functions
	m_GLFW_window = glfwCreateWindow(640, 320, "LearnOpenGL", NULL, NULL);
	glfwMakeContextCurrent(m_GLFW_window);
	if (m_GLFW_window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
	}

	// Set the required callback functions
	//glfwSetKeyCallback(window, key_callback);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize OpenGL context" << std::endl;
	}

	// Define the viewport dimensions
	glViewport(0, 0, Emulator::WIDTH * 10, Emulator::HEIGHT * 10);

	// Create Vertex Array Object
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Create a Vertex Buffer Object and copy the vertex data to it
	GLuint vbo;
	glGenBuffers(1, &vbo);
	GLfloat vertices[] = {
		//  Position   Color             Texcoords
		-1.f,  1.f, 0.0f, 0.0f, // Top-left
		1.f,  1.f, 1.0f, 0.0f, // Top-right
		1.f, -1.f, 1.0f, 1.0f, // Bottom-right
		-1.f, -1.f, 0.0f, 1.0f  // Bottom-left
	};

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Create an element array
	GLuint ebo;
	glGenBuffers(1, &ebo);

	GLuint elements[] = {
		0, 1, 2,
		2, 3, 0
	};
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

	// Create and compile the vertex shader
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &m_vertexShader, NULL);
	glCompileShader(vertexShader);

	// Create and compile the fragment shader
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &m_pixelShader, NULL);
	glCompileShader(fragmentShader);

	// Link the vertex and fragment shader into a shader program
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);

	// Specify the layout of the vertex data
	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

	GLint colAttrib = glGetAttribLocation(shaderProgram, "uv");
	glEnableVertexAttribArray(colAttrib);
	glVertexAttribPointer(colAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	float color[] = { 1.0f, 0.0f, 0.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glfwSwapInterval(1);
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{

	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_1:
			Emulator::m_Input[0x1] = '1';
			break;
		case GLFW_KEY_2:
			Emulator::m_Input[0x2] = '1';
			break;
		case GLFW_KEY_3:
			Emulator::m_Input[0x3] = '1';
			break;
		case GLFW_KEY_4:
			Emulator::m_Input[0xC] = '1';
			break;
		case GLFW_KEY_Q:
			Emulator::m_Input[0x4] = '1';
			break;
		case GLFW_KEY_W:
			Emulator::m_Input[0x5] = '1';
			break;
		case GLFW_KEY_E:
			Emulator::m_Input[0x6] = '1';
			break;
		case GLFW_KEY_R:
			Emulator::m_Input[0xD] = '1';
			break;
		case GLFW_KEY_A:
			Emulator::m_Input[0x7] = '1';
			break;
		case GLFW_KEY_S:
			Emulator::m_Input[0x8] = '1';
			break;
		case GLFW_KEY_D:
			Emulator::m_Input[0x9] = '1';
			break;
		case GLFW_KEY_F:
			Emulator::m_Input[0xE] = '1';
			break;
		case GLFW_KEY_Z:
			Emulator::m_Input[0xA] = '1';
			break;
		case GLFW_KEY_X:
			Emulator::m_Input[0x0] = '1';
			break;
		case GLFW_KEY_C:
			Emulator::m_Input[0xB] = '1';
			break;
		case GLFW_KEY_V:
			Emulator::m_Input[0xF] = '1';
			break;
		default:
			break;
		}
	}
	else if (action == GLFW_RELEASE)
	{
		switch (key)
		{
		case GLFW_KEY_1:
			Emulator::m_Input[0x1] = '0';
			break;
		case GLFW_KEY_2:
			Emulator::m_Input[0x2] = '0';
			break;
		case GLFW_KEY_3:
			Emulator::m_Input[0x3] = '0';
			break;
		case GLFW_KEY_4:
			Emulator::m_Input[0xC] = '0';
			break;
		case GLFW_KEY_Q:
			Emulator::m_Input[0x4] = '0';
			break;
		case GLFW_KEY_W:
			Emulator::m_Input[0x5] = '0';
			break;
		case GLFW_KEY_E:
			Emulator::m_Input[0x6] = '0';
			break;
		case GLFW_KEY_R:
			Emulator::m_Input[0xD] = '0';
			break;
		case GLFW_KEY_A:
			Emulator::m_Input[0x7] = '0';
			break;
		case GLFW_KEY_S:
			Emulator::m_Input[0x8] = '0';
			break;
		case GLFW_KEY_D:
			Emulator::m_Input[0x9] = '0';
			break;
		case GLFW_KEY_F:
			Emulator::m_Input[0xE] = '0';
			break;
		case GLFW_KEY_Z:
			Emulator::m_Input[0xA] = '0';
			break;
		case GLFW_KEY_X:
			Emulator::m_Input[0x0] = '0';
			break;
		case GLFW_KEY_C:
			Emulator::m_Input[0xB] = '0';
			break;
		case GLFW_KEY_V:
			Emulator::m_Input[0xF] = '0';
			break;
		default:
			break;
		}
	}
}

void Drop_CallBack(GLFWwindow* window, int count, const char ** paths)
{
	for (int i = 0; i < count; i++)
	{
		Emulator::s_CurRom = paths[i];
	}

	Emulator::s_ReadData = true;
}

void glfw::SetInputCallBack()
{
	glfwSetKeyCallback(m_GLFW_window, key_callback);
	glfwSetDropCallback(m_GLFW_window, Drop_CallBack);
}


void glfw::Loop()
{
	std::cout << "loop";
	// Game loop
	while (!glfwWindowShouldClose(m_GLFW_window))
	{
		glfwPollEvents();

		if (Emulator::s_ReadData)
		{
			m_Emulator->ReadInData();
		}

		for (int j = 0; j < m_Emulator->GetCurrentSpeed(); ++j)
		{
			m_Emulator->GameLoop();
		}

		// Clear the screen to black
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_Emulator->GetMemWidth() , m_Emulator->GetMemheight(), 0, GL_RGB, GL_FLOAT, &m_Emulator->GetGraphMem()[0]);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		// Swap the screen buffers
		glfwSwapBuffers(m_GLFW_window);
		
	}

}