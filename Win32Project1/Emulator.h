#pragma once
#define GLEW_STATIC

#include <vector>

typedef unsigned char U8;
typedef unsigned short U16;

class Emulator
{
public:

	struct Color
	{
		float R, G, B;
		Color() : R(0), G(0), B(0) {}
		Color(float r, float g, float b) : R(r), G(g), B(b) {}
		void Flip() { R = !R; G = !G; B = !B; }
		bool operator==(float f) { return  R == f; }
		void Clear() { R = G = B = 0; }
	};

	

	Emulator();
	~Emulator();

	int Main();

	static U8 m_Input[16];
	static std::string s_CurRom;
	static bool s_ReadData;

	static U8 WIDTH;
	static U8 HEIGHT;

	std::vector<Color> GetGraphMem()
	{
		return m_graphMemory;
	}


	void ReadInData();
	void GameLoop();
	U8 GetMemWidth();
	U8 GetMemheight();
	U8 GetCurrentSpeed();

private:

	short GetCurrentOpCode();

	U8 m_graphMemWidth, m_graphMemheigth ;
	U8 m_Memory[4096];
	U8 m_GPR[16]; //V0 -> VF
	U8 m_RPL[8];
	std::vector<Color> m_graphMemory;

	U16 m_Register[16];
	U16 m_RegI;
	U16 m_PC;

	//64 * 32
	U8 m_DelayTimer;
	U8 m_SoundTimer;
	U8 m_StackPointr;
	U8 m_Speed;
	bool m_DrawFlag;
	bool m_SkipCounter;
	bool m_SupaChipMode;
	

	bool m_WrapPixels;
	bool m_AnimalRace;

};

