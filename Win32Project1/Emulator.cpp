//Headers
#include "Emulator.h"
#include "Font.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>

using namespace std;

U8 Emulator::m_Input[16];
string Emulator::s_CurRom = ".//Games//MAZE";
bool Emulator::s_ReadData = true;
U8 Emulator::WIDTH = 64;
U8 Emulator::HEIGHT = 32;

Emulator::Emulator() :
	m_graphMemheigth(HEIGHT),
	m_graphMemWidth(WIDTH),
	m_RegI(0),
	m_PC(0x200),
	m_DelayTimer(0),
	m_SoundTimer(0),
	m_StackPointr(0),
	m_DrawFlag(false),
	m_SkipCounter(false),
	m_SupaChipMode(false)
{

}

Emulator::~Emulator()
{

}

void Emulator::ReadInData()
{
	m_graphMemory.clear();

	for (U8 &byte : m_Memory)
	{
		byte = 0;
	}
	m_Speed = 8;
	m_SupaChipMode = false;
	m_graphMemheigth = HEIGHT;
	m_graphMemWidth = WIDTH;
	m_graphMemory.resize(m_graphMemheigth*m_graphMemWidth);
	m_RegI = 0;
	m_PC = 0x200;
	string inputAdress = s_CurRom;
	ifstream inputFile;
	inputFile.open(inputAdress, ios::binary | ios::in);

	string dataFile((istreambuf_iterator<char>(inputFile)), istreambuf_iterator<char>());
	memcpy(&m_Memory[512], &dataFile[0], dataFile.size());

	memcpy(m_Memory, chip8_fontset, sizeof(U8) * 80);


	U16 totalSum = 0;
	for (U8 byte : m_Memory)
	{
		totalSum += byte;
	}

	//[Blitz = 53076, Connect4 = 54642, AnimalRace = 50799]

	if (totalSum == 53076) //blitz should ignore pixels
	{
		m_WrapPixels = false;
	}
	else //other games should wrap
	{
		m_WrapPixels = true;
	}

	if (totalSum == 50799)
	{
		m_AnimalRace = true;
	}
	else
	{
		false;
	}

	for (U16 i = 0; i < m_graphMemWidth * m_graphMemheigth; i++)
	{
		m_graphMemory[i].Clear();
	}

	for (int i = 0; i < 0xf; i++)
	{
		m_Input[i] = '0';
	}

	for (size_t i = 0; i < 16; i++)
	{
		m_GPR[i] = 0;
	}

	m_DrawFlag = true;
	s_ReadData = false;
}

short Emulator::GetCurrentOpCode()
{
	unsigned short opCode = 0;
	opCode = m_Memory[m_PC];
	opCode <<= 8;
	opCode |= m_Memory[m_PC + 1];
	m_PC += 2;
	cout << hex << opCode << endl;
	return opCode;
}

void Emulator::GameLoop()
{
	//UPDATE TIMERS
	if (m_DelayTimer > 0)
		--m_DelayTimer;

	if (m_SoundTimer > 0)
	{
		if (m_SoundTimer == 1)
			//BEEP
			--m_SoundTimer;
	}


	U16 curOpcode = GetCurrentOpCode();
	switch (curOpcode >> 12)
	{
	case 0x0:
		switch (curOpcode & 0x00FF)
		{
		case 0xEE://returns from subroutine
			--m_StackPointr;
			m_PC = m_Register[m_StackPointr];
			break;

		case 0xE0://clear screen
			for (U16 i = 0; i < m_graphMemory.size(); i++)
			{
				m_graphMemory[i].Clear();
			}
			m_DrawFlag = true;
			break;

		case  0xFF: //turn to supa chip
			m_graphMemheigth = HEIGHT * 2;
			m_graphMemWidth = WIDTH * 2;
			memcpy(&m_Memory[80], chip8_SuperFont, sizeof(U8) * 160);
			m_graphMemory.resize(m_graphMemheigth*m_graphMemWidth);

			m_Speed = 16;
			m_SupaChipMode = true;
			break;

		case 0xFE: //turn off supa chip

			m_Speed = 8;
			m_graphMemheigth = HEIGHT;
			m_graphMemWidth = WIDTH;
			m_graphMemory.resize(m_graphMemheigth*m_graphMemWidth);
			m_SupaChipMode = false;
			break;

		case 0x00FB: //Scroll display 4 pixels to the right
			{
				for (int x = m_graphMemWidth - 4; x >= 0; --x) {
					for (int y = 0; y < m_graphMemheigth; y++) {
						m_graphMemory.at(((x + 4) % m_graphMemWidth) + (y * m_graphMemWidth)) = m_graphMemory.at((x % m_graphMemWidth) + (y * m_graphMemWidth));
						m_graphMemory.at((x % m_graphMemWidth) + (y * m_graphMemWidth)).Clear();
					}
				}
			}
			break;

		case 0x00FC: //Scroll display 4 pixels to the left
			{
				for (int x = 4; x < m_graphMemWidth; x++) {
					for (int y = 0; y < m_graphMemheigth; y++) {
						m_graphMemory.at(((x - 4) % m_graphMemWidth) + (y * m_graphMemWidth)) = m_graphMemory.at((x % m_graphMemWidth) + (y * m_graphMemWidth));
						m_graphMemory.at((x % m_graphMemWidth) + (y * m_graphMemWidth)).Clear();
					}
				}

			}
			break;


		}

		if ((curOpcode & 0x00F0) >> 4 == 0xC) //Scroll display N lines down
		{
			int n = curOpcode & 0x000F;

			for (int y = m_graphMemheigth; y >= 0; --y) {
				if (y + n < m_graphMemheigth) {
					for (int x = 0; x < m_graphMemWidth; x++) {
						m_graphMemory.at((x % m_graphMemWidth) + ((y + n) * m_graphMemWidth)) = m_graphMemory.at((x % m_graphMemWidth) + (y * m_graphMemWidth));
						m_graphMemory.at((x % m_graphMemWidth) + (y * m_graphMemWidth)).Clear();
					}
				}
			}
		}
		break;

	case 0x1://Jumps to address NNN.
		m_PC = curOpcode & 0x0FFF;
		break;
	case 0x2:    //Calls subroutine at NNN.
		m_Register[m_StackPointr] = m_PC;
		m_StackPointr++;
		m_PC = curOpcode & 0x0FFF;
		break;
	case 0x3://Skips the next instruction if VX equals NN.
	{
		U8 X = (curOpcode & 0x0F00) >> 8;
		if (m_GPR[X] == (curOpcode & 0x00FF))
			m_PC += 2;
		break;
	}

	case 0x4://Skips the next instruction if VX doesn't equal NN.
	{
		U8 X = (curOpcode & 0x0F00) >> 8;
		if (m_GPR[X] != (curOpcode & 0x00FF))
			m_PC += 2;
		break;
	}

	case 0x5://Skips the next instruction if VX equals VY.
	{
		U8 X = (curOpcode & 0x0F00) >> 8;
		U8 Y = (curOpcode & 0x00F0) >> 4;
		if (m_GPR[X] == m_GPR[Y])
			m_PC += 2;
		break;
	}

	case 0x6://Sets VX to NN
	{
		U8 X = (curOpcode & 0x0F00) >> 8;
		m_GPR[X] = (curOpcode & 0x00FF);
		break;
	}

	case 0x7://Adds NN to VX.
	{
		U8 X = (curOpcode & 0x0F00) >> 8;
		m_GPR[X] += (curOpcode & 0x00FF);
		break;
	}

	case 0x8:
	{
		U8 X = (curOpcode & 0x0F00) >> 8;
		U8 Y = (curOpcode & 0x00F0) >> 4;
		switch (curOpcode & 0x000F)
		{
		case 0x0://Sets VX to the value of VY.
			m_GPR[X] = m_GPR[Y];
			break;
		case 0x1://Sets VX to VX or VY.
			m_GPR[X] = m_GPR[X] | m_GPR[Y];
			break;
		case 0x2://Sets VX to VX and VY.
			m_GPR[X] = m_GPR[Y] & m_GPR[X];
			break;
		case 0x3://Sets VX to VX xor VY.
			m_GPR[X] = m_GPR[X] ^ m_GPR[Y];
			break;
		case 0x4://Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
			if (m_GPR[Y] > 0xFF - m_GPR[X])
				m_GPR[0xF] = 1;
			else
				m_GPR[0xF] = 0;
			m_GPR[X] = m_GPR[Y] + m_GPR[X];
			break;
		case 0x5: //VY is subtracted from VX.VF is set to 0 when there's a borrow, and 1 when there isn't.
			if (m_GPR[X] < m_GPR[Y])
				m_GPR[0xF] = 0;
			else
				m_GPR[0xF] = 1;

			m_GPR[X] -= m_GPR[Y];
			break;
		case 0x6: //Shifts VX right by one.VF is set to the value of the least significant bit of VX before the shift.[2]
			m_GPR[0xF] = m_GPR[X] & 0x1;
			m_GPR[X] = m_GPR[X] >> 1;
			break;

		case 0x7://Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
			m_GPR[X] = m_GPR[Y] - m_GPR[X];
			if (m_GPR[Y] < m_GPR[X])
				m_GPR[0xF] = 0;
			else
				m_GPR[0xF] = 1;
			break;

		case 0xE://Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift.[2]
			m_GPR[0xF] = m_GPR[X] >> 7;
			m_GPR[X] = m_GPR[X] << 1;
			break;

		default:
			break;
		}
	}
	break;

	case 0x9: //Skips the next instruction if VX doesn't equal VY.
	{
		U8 X = (curOpcode & 0x0F00) >> 8;
		U8 Y = (curOpcode & 0x00F0) >> 4;
		if (m_GPR[X] != m_GPR[Y])
			m_PC += 2;
		break;
	}

	case 0xA: //Sets I to the address NNN.
		m_RegI = curOpcode & 0x0FFF;
		break;

	case 0xB: //Jumps to the address NNN plus V0.
		m_PC = (curOpcode & 0x0FFF) + m_GPR[0];
		break;

	case 0xC: //Sets VX to the result of a bitwise and operation on a random number and NN.
	{
		U8 X = (curOpcode & 0x0F00) >> 8;
		m_GPR[X] = (rand() % 256) & (curOpcode & 0x00FF);
		break;
	}

	case 0xD: //Show N - byte sprite from M(I) at coords(VX, VY), VF : = collision. If N = 0 and extended mode, show 16x16 sprite.
	{
		U8 X = m_GPR[(curOpcode & 0x0F00) >> 8];
		U8 Y = m_GPR[(curOpcode & 0x00F0) >> 4];

		U8 heigth = (curOpcode & 0x000F);
		U8 width = 8;
		U8 pixel;

		m_GPR[0xF] = 0;

		if (heigth == 0 )
		{
			for (U8 y = 0; y < 16; y++)
			{
				pixel = m_Memory[m_RegI + y * 2];
				for (int x = 0; x < 8; x++) {

					int pixX = (X + x) % m_graphMemWidth;
					int pixY = (Y + y) % m_graphMemheigth;

					if ((pixel & (0x80 >> x)) != 0) {
						if (m_graphMemory.at(pixX + (pixY * m_graphMemWidth)) == 1)
							m_GPR[0xF] = 1;
						m_graphMemory.at(pixX + (pixY * m_graphMemWidth)).Flip();
					}
				}

				pixel = m_Memory[m_RegI + 1 + y * 2];
				for (int x = 0; x < 8; x++) {

					int pixX = (X + x + 8) % m_graphMemWidth;
					int pixY = (Y + y) % m_graphMemheigth;

					if ((pixel & (0x80 >> x)) != 0) {
						if (m_graphMemory.at(pixX + (pixY * m_graphMemWidth)) == 1)
							m_GPR[0xF] = 1;
						m_graphMemory.at(pixX + (pixY * m_graphMemWidth)).Flip();
					}
				}
			}
		}
		else
		{
			for (int y = 0; y < heigth; y++)
			{
				pixel = m_Memory[m_RegI + y];
				for (int x = 0; x < 8; x++)
				{
					if ((pixel & (0x80 >> x)) != 0)
					{
						if (!m_WrapPixels && (X + x >= m_graphMemWidth || Y + y >= m_graphMemheigth))
							break;

						int pixX = (X + x) % m_graphMemWidth;
						int pixY = (Y + y) % m_graphMemheigth;

						if (m_graphMemory.at(pixX + (pixY * m_graphMemWidth)) == 1)
							m_GPR[0xF] = 1;

						m_graphMemory.at(pixX + (pixY * m_graphMemWidth)).Flip();
					}
				}
			}
		}


		break;
	}

	case 0xE: //Skips the next instruction if the key stored in VX is pressed.
	{
		U8 X = (curOpcode & 0x0F00) >> 8;

		switch (curOpcode & 0x00FF)
		{
		case 0x9E: //Skips the next instruction if the key stored in VX is pressed.

			if (m_Input[m_GPR[X]] == '1')
				m_PC += 2;

			break;
		case 0xA1://Skips the next instruction if the key stored in VX isn't pressed.
			if (m_Input[m_GPR[X]] == '0')
				m_PC += 2;

			break;
		}

	}
	break;
	case 0xF:
	{
		U8 X = (curOpcode & 0x0F00) >> 8;

		switch (curOpcode & 0x00FF)
		{

		case 0x07: //Sets VX to the value of the delay timer.
			m_GPR[X] = m_DelayTimer;
			break;
		case 0x0A://A key press is awaited, and then stored in VX.

		{
			bool keyPressed = false;

			for (U8 i = 0; i < 16; i++)
			{
				if (m_Input[i] == '1') {
					m_GPR[X] = (unsigned char)i;
					keyPressed = true;
				}
			}

			if (!keyPressed)
				m_PC -= 2;
		}
		break;

		case 0x15://Sets the delay timer to VX.
			m_DelayTimer = m_GPR[X];
			break;
		case 0x18://Sets the sound timer to VX.
			m_SoundTimer = m_GPR[X];
			break;
		case 0x1E://Adds VX to I.[3]
			m_RegI += m_GPR[X];
			if (m_RegI > 0xFFF) //overflow
				m_GPR[0xf] = 1;
			else
				m_GPR[0xf] = 0;
			break;
		case 0x29:
			//Sets I to the location of the sprite for the character in 
			//VX.Characters 0 - F(in hexadecimal) are represented by a 4x5 font.
			m_RegI = m_GPR[X] * 5;
			break;

		case 0x30:    //Point I to 10 - byte font sprite for digit VX(0..9)
			m_RegI = 80 + X * 10;
			break;
		case 0x33:
		{
			U8 ones, tens, hundreds;
			U8 value = m_GPR[X];
			ones = value % 10;
			value = value / 10;
			tens = value % 10;
			hundreds = value / 10;
			m_Memory[m_RegI] = hundreds;
			m_Memory[m_RegI + 1] = tens;
			m_Memory[m_RegI + 2] = ones;
			break;
		}
		default:
			break;

		case 0x55: //Stores V0 to VX in memory starting at address I.[4]
			for (int i = 0x0; i <= X; ++i)
			{
				m_Memory[m_RegI + i] = m_GPR[i];
			}
			if (m_AnimalRace)
				m_RegI += X + 1;
			break;

		case 0x65://Fills V0 to VX with values from memory starting at address I.[4]
			for (int i = 0x0; i <= X; ++i)
			{
				m_GPR[i] = m_Memory[m_RegI + i];
			}
			if (m_AnimalRace)
				m_RegI += X + 1;
			break;

			//Stores the Binary-coded decimal representation of VX, with the most significant of three digits at the address in I, 
			//the middle digit at I plus 1, and the least significant digit at I plus 2. 
			//(In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I, 
			//the tens digit at location I+1, and the ones digit at location I+2.)

		case 0x75://    Store V0..VX in RPL user flags(X <= 7)

			for (U8 i = 0; i <= X; i++)
			{
				m_RPL[i] = m_GPR[i];
			}

			break;

		case	0x85: //    Read V0..VX from RPL user flags(X <= 7)

			for (U8 i = 0; i <= X; i++)
			{
				m_GPR[i] = m_RPL[i];
			}

			break;
		}


	}
	break;

	default:
		break;
	}
}

U8 Emulator::GetMemWidth()
{
	return m_graphMemWidth;
}

U8 Emulator::GetMemheight()
{
	return m_graphMemheigth;
}

U8 Emulator::GetCurrentSpeed()
{
	return m_Speed;
}