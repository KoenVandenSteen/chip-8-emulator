#pragma once

#include <glad\glad.h>
#include <GLFW\glfw3.h>

class Emulator;


class glfw
{
public:
	glfw();
	~glfw();


	// Shader sources
	const GLchar* m_vertexShader =
		"#version 150 core\n"
		"in vec2 position;"
		"in vec2 uv;"
		"out vec2 UV;"
		"void main() {"
		"UV = uv;"
		"   gl_Position = vec4(position, 0.0, 1.0);"
		"}";
	const GLchar* m_pixelShader =
		"#version 150 core\n"
		"out vec4 outColor;"
		"in vec2 UV;"
		"uniform sampler2D tex;"
		"void main() {"
		"   outColor = texture(tex, UV) * vec4(gl_FragCoord.r/640, 0.5f, 1, 1);"
		"}";




private:
	Emulator *m_Emulator;

	void InitializeGLFW();
	void SetInputCallBack();
	void Loop();

	GLFWwindow* m_GLFW_window = nullptr;
};

